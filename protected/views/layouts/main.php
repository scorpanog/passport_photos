<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />
	<META Name="description" CONTENT="<?php echo (isset($this->meta_desc) ? CHtml::encode($this->meta_desc) : ''); ?>">

	<!-- blueprint CSS framework -->
	<!-- <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection" /> -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print" />
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
	<![endif]-->

	<!-- <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" /> -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen_style.css" />

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body class="gridLayout">

<div class="container" id="container">
<div class="contentWrapper">
	<div id="header"> 
    <div class="logo"><h1><a href="#">CanadianPassportPhotos.ca</a></h1></div>
    <div class="headerMenu">
    <ul>
        <li><a href="index.html">Home</a></li>
        <li><a href="site_contact.html">Contact us</a></li>
    </ul>
    <!-- end headerMenu --></div>
  <!-- end #header --></div>

	<div id="globalNavigation">
		<?php 
		$this->widget('zii.widgets.CMenu',array(
			'items'=>array(
				array('label'=>'About us', 'url'=>array('/site/about_us')),
				array('label'=>'Locations', 'url'=>array('/store/locator')),
				array('label'=>'Resources', 'url'=>array('/site/resources')),
				array('label'=>'Photos on-the-go', 'url'=>array('/site/onthego')),
				array('label'=>'add store', 'url'=>array('/store/create'),'visible'=>!Yii::app()->user->isGuest),
				//array('label'=>'Login', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
				//array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest)
			),
		)); 
		?>
	</div><!-- mainmenu -->
	<br class="clearfloat" />
	<?php /* if(isset($this->breadcrumbs)):?>
		<?php $this->widget('zii.widgets.CBreadcrumbs', array(
			'links'=>$this->breadcrumbs,
		)); ?><!-- breadcrumbs -->
	<?php endif */?>

	<?php echo $content; ?>

	<div class="clear"></div>

<br class="clearfloat" />
    <!-- Begin Footer -->
    <!---------------------------------------------------------------------------->
    <div id="footer">
    <div class="footerMenu">
        <ul>     
            <li>Copyright &copy; 2010 CanadianPassportPhotos.ca. All rights reserved.</li>
           <!-- <li><a href="site_privacy.html">Privacy</a></li>-->
            <li><span style="text-align: right"><a href="site_contact.html">Contact us</a></span></li>
        </ul>    
    </div>
    <!-- end #footer --></div>
	
<!----------------------------------------------------------------->
</div>

</div><!-- page -->

</body>
</html>
